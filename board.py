"""A representation board for the tic-tac-toe game"""

import random

class TicTacToeBoard:
    """The tic-tac-toe board."""

    def __init__(self):
        """Construction."""
        self.board = ["."]*9
        self.turn = "X"
        self.draw = False
        self.end = False
        self.posList = [1,2,3,4,5,6,7,8,9]

    def mark(self, symbol, position):
        """Mark a symbol at a position."""
        self.board[position-1] = symbol
        self.posList.remove(position)
        return self.terminate()

    def ChangePlayer(self):
        if self.turn =="X":
            self.turn = "O"
            return self.Botplay()
        else:
            self.turn = "X"

    def drawBoard(self):
        print(self.board[0]+" "+self.board[1]+" "+self.board[2])
        print(self.board[3]+" "+self.board[4]+" "+self.board[5])
        print(self.board[6]+" "+self.board[7]+" "+self.board[8])
        
    def Botplay(self):
        botRandomPos = random.choice(self.posList)
        self.board[botRandomPos - 1] = self.turn
        self.posList.remove(botRandomPos)
        self.drawBoard()
        return self.terminate()
    
    def terminate(self):
        """Determine if the game is over"""
        if self.board[0] == self.board[1] == self.board[2] != ".":
            self.end = True
            return self.end
        elif self.board[3] == self.board[4] == self.board[5] != ".":
            self.end = True
            return self.end
        elif self.board[6] == self.board[7] == self.board[8] != ".":
            self.end = True
            return self.end
        elif self.board[0] == self.board[3] == self.board[6] != ".":
            self.end = True
            return self.end
        elif self.board[1] == self.board[4] == self.board[7] != ".":
            self.end = True
            return self.end
        elif self.board[2] == self.board[5] == self.board[8] != ".":
            self.end = True
            return self.end
        elif self.board[0] == self.board[4] == self.board[8] != ".":
            self.end = True
            return self.end
        elif self.board[2] == self.board[4] == self.board[6] != ".":
            self.end = True
            return self.end
        elif "." not in self.board:
            self.end = True
            self.draw = True
            return self.end

        return self.ChangePlayer()